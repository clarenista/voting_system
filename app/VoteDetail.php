<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoteDetail extends Model
{
    public function voter(){
        return $this->belongsTo('App\Voter', 'voter_code', 'voter_code');
    }
    
    public function candidate(){
        return $this->belongsTo('App\Candidate', 'candidate_id', 'id');

    }
}
