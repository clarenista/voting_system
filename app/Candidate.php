<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\VoteDetail;

class Candidate extends Model
{
    protected $fillable = ['first_name', 'last_name', 'dp', 'organization'];

    function candidate_votes(){
    	return $this->hasMany(VoteDetail::class, 'candidate_id', 'id');
    }
}
