<?php

namespace App\Exports;

use App\Voter;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Carbon\Carbon;

class VotersExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Voter::withCount('votes')->get();
    }
    public function map($registration) : array {
    	
        return [
            $registration->organization,
            $registration->first_name,
            $registration->last_name,
            $registration->designation,
            self::renderisEligible($registration->isEligible),
            $registration->email,
            $registration->username,
            $registration->voter_code,
            $registration->votes_count,
        ] ;
 
 
    }   

    public function headings() : array {
        return [
           'ORGANIZATION',
           'FIRST NAME',
           'LAST NAME',
           'CLASSIFICATION',
           'ELIGIBILITY',
           'EMAIL ADDRESS',
           'USERNAME',
           'VOTER CODE',
           'VOTES COUNT',
        ] ;
    }    
    private function renderisEligible($el){
        switch ($el) {
          case 0:
            return "Uneligible";
            break;
          default:
            return "Eligible";
            break;
        }
      }
}
