<?php

namespace App\Exports;

use App\Voter;
use App\VoteDetail;
use App\Candidate;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Carbon\Carbon;

class VotersVoteToExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    protected $candidate_id;

    function __construct($candidate_id) {
            $this->candidate_id = $candidate_id;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        
        return \App\VoteDetail::with('voter', 'candidate')->where('candidate_id', $this->candidate_id)->get(['candidate_id', 'voter_code']);
        
    }
    public function map($vote_details) : array {
    	
        return [
            $vote_details->voter->first_name,
            $vote_details->voter->last_name,
            $vote_details->candidate->first_name,
            $vote_details->candidate->last_name,
        ] ;
 
 
    }  

    public function headings() : array {
        return [
           'VOTER\'S FIRST NAME',
           'VOTER\'S LAST NAME',
           'CANDIDATES\'S FIRST NAME',
           'CANDIDATES\'S LAST NAME',
        ] ;
    }
}
