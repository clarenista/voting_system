<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\VoteDetail;

class Voter extends Model
{
    protected $fillable = ['registrant_id', 'first_name', 'last_name', 'email', 'username', 'organization', 'isEligible','voter_code', 'designation'];

    public function votes(){
        return $this->hasMany('App\VoteDetail', 'voter_code', 'voter_code');
    }
}
