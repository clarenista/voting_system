<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VotingCode extends Model
{
    protected $fillable = ['voting_code'];

    function voter(){
        return $this->belongsTo('App\Voter', 'voting_code', 'voter_code');
    }
}
