<?php

namespace App\Http\Controllers;

use App\User;
use App\Setup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class SetupController extends Controller
{
    public function index(){
        $candidates = DB::table('candidates')->get();
        $setup = DB::table('setups')->first();
    	$adminExist = DB::table('users')->count();
    	if(!$adminExist){
    		return view('setup.index');
    	}else{
    		return view('home.index', ['candidates' => $candidates, 'setup' => $setup]);
    	}
    }

    public function save(Request $request){
        $company_name = $request->company_name;
    	$vote_count = $request->vote_count;
    	$company_logo = $request->file_logo;
        $imageName = $request->company_name.'.'.$request->file_logo->getClientOriginalExtension();
        $request->file_logo->move(public_path('uploads/logo'), $imageName);

    	$file_event_logo = $request->file_event_logo;
        $eventImageName = 'event.'.$request->file_event_logo->getClientOriginalExtension();
        $request->file_event_logo->move(public_path('uploads/logo'), $eventImageName);

    	$setup = DB::table('setups')
    		->insert([
                'company_name' => $company_name,
    			'vote_count' => $vote_count,
    			'company_logo' => '/uploads/logo/'.$request->company_name.'.'.$request->file_logo->getClientOriginalExtension(),
    			'file_event_logo' => '/uploads/logo/event.'.$request->file_event_logo->getClientOriginalExtension(),
    		]);

    	// saving admin
    	$name = $request->name;
    	$email = $request->email;
    	$password = Hash::make($request->password);
    	$admin = DB::table('users')
    		->insert([
    			'name' => $name,
    			'email' => $email,
    			'password' => $password,
    		]);

    }

    function show(){
        $setup = DB::table('setups')->first();
        return response()->json([
            'setup' => $setup
        ]);
    }
}
