<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class VoteDetailController extends Controller
{
    function index(Request $request){
        $settings = DB::table('settings')->first();
        $start_dt = Carbon::parse(date($settings->start_period));
        $start_dt = Carbon::parse(date($start_dt));
        // $start_dt->setTimezone('Asia/Manila');
        // dd($start_dt->lessThan(Carbon::now('-8:00')));
        // dd(Carbon::parse(date('Y-m-d H:i:s')));
        // dd($start_dt->lessThan(Carbon::parse(date('Y-m-d H:i:s'))));
        $end_dt = Carbon::parse(date($settings->end_period));         
        $end_dt = Carbon::parse(date($end_dt));         
        // dd($end_dt->lessThan(Carbon::parse(date('Y-m-d H:i:s'))));
        // dd($start_dt->lessThan(Carbon::parse(date('Y-m-d H:i:s'))));
        if($start_dt->lessThan(Carbon::parse(date('Y-m-d H:i:s')))){
            if($end_dt->greaterThan(Carbon::parse(date('Y-m-d H:i:s')))){
               return view('vote.index', ['candidates' => DB::table('candidates')->get(), 'settings' => json_encode($settings)]);
            }else{
                return view('period_message', ['message' => json_encode($settings->closed_message)]);
            }
        }else{
            return view('period_message', ['message' => json_encode($settings->not_yet_message)]);

        }        
    }

    function save(Request $request){
        $settings = DB::table('settings')->first();
        $setup = DB::table('setups')->first();
        $start_dt = Carbon::parse(date($settings->start_period));
        $start_dt = Carbon::parse(date($start_dt));
        $end_dt = Carbon::parse(date($settings->end_period)); 
        $end_dt = Carbon::parse(date($end_dt)); 
        if($start_dt->lessThan(Carbon::parse(date('Y-m-d H:i:s')))){
            if($end_dt->greaterThan(Carbon::parse(date('Y-m-d H:i:s')))){
                $votes = json_decode($request->votes);
                $exists = DB::table('vote_details')->where('voter_code', $request->vote_code)->exists();
                if(!$exists){
                    $voteCode = DB::table('voters')->where('voter_code', $request->vote_code)->where('isEligible' , 1)->exists();
                    if($voteCode){
                        if(count($votes) <= $setup->vote_count){
                            foreach ($votes as $key => $value) {
                                $save = DB::table('vote_details')
                                    ->insert([
                                        'voter_code' => $request->vote_code,
                                        'candidate_id' => $value->id,
                                        'created_at' => \Carbon\Carbon::now(),
                                        'updated_at' => \Carbon\Carbon::now(),
                                    ]);
                            }
                        }
                    }
                }
            }else{
                return 'Voting is closed';
            }
        }else{
            return 'Voting is not yet ready';
        }        

    }
}
