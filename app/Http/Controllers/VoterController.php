<?php

namespace App\Http\Controllers;

use App\Voter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Exports\VotersExport;
use App\Exports\VotersVoteToExport;
use Maatwebsite\Excel\Facades\Excel;

class VoterController extends Controller
{
    function checkVoteCode(Request $request)
    {
        $candidates = DB::table('candidates')->get();
        // $voteCode = DB::table('voters')->where('voter_code', $request->vote_code)->where('isEligible' , 1)->exists();
        $voter = DB::table('voters')->where('voter_code', $request->vote_code)->where('isEligible', 1)->first();
        $myVotes = DB::table('vote_details')->where('voter_code', $request->vote_code)->get();
        foreach ($candidates as $key => $value) {
            $candidates[$key]->iVoted = DB::table('vote_details')->where('candidate_id', $value->id)->where('voter_code', $request->vote_code)->exists();
        }
        return response()->json([
            'voter' => $voter,
            'candidates' => $candidates,
            'myVotes' => $myVotes,
        ]);
    }
    function checkusername(Request $request)
    {
        $candidates = DB::table('candidates')->get();
        // $voteCode = DB::table('voters')->where('voter_code', $request->vote_code)->where('isEligible' , 1)->exists();
        $voter = DB::table('voters')->where('email', 'like', '%' . $request->username . '%')->where('isEligible', 1)->first();
        // dd($voter);
        if ($voter) {

            $myVotes = DB::table('vote_details')->where('voter_code', $voter->voter_code)->get();
            foreach ($candidates as $key => $value) {
                $candidates[$key]->iVoted = DB::table('vote_details')->where('candidate_id', $value->id)->where('voter_code', $voter->voter_code)->exists();
            }
            return response()->json([
                'voter' => $voter,
                'candidates' => $candidates,
                'myVotes' => $myVotes,
            ]);
        } else {
            return response('', 401);
        }
    }


    function index()
    {
        return view('voters.index', ['voters' => DB::table('voters')->orderBy('id', 'desc')->get()->take(10), 'available_vc' => DB::table('voting_codes')->where('isTaken', 0)->count()]);
    }

    function save(Request $request)
    {
        // $code =Str::random(4);
        // $isVoteCodeExists = DB::table('voters')->where('voter_code', $code)->exists();
        // if($isVoteCodeExists){
        //     $vote_code = Str::random(4);
        // }        
        $voter = new Voter();
        // $vote_code = $this->validateVoteCode($code);
        $voter->organization = $request->organization;
        $voter->first_name = $request->first_name;
        $voter->last_name = $request->last_name;
        $voter->designation = $request->designation;
        $voter->email = $request->email;
        $voter->isEligible = $request->isEligible;
        $voter->voter_code = '';
        $voter->save();

        return response()->json([
            'voter' => $voter
        ]);
    }

    function update(Request $request)
    {
        $voter = Voter::find($request->id);
        $voter->organization = $request->organization;
        $voter->first_name = $request->first_name;
        $voter->last_name = $request->last_name;
        $voter->designation = $request->designation == '' ? null : $request->designation;
        $voter->email = $request->email;
        $voter->isEligible = $request->isEligible;
        // $voter->voter_code = '';  //add validation later
        if ($request->isEligible == 0) {
            $this->deleteVotingCode($voter);
            $voter->voter_code = null;  //add validation later
        }
        $voter->save();

        // $votingCodeUpdate = DB::table('voting_codes')
        //     ->where('voting_code', $voting_code)
        //     ->update([
        //         'isTaken' => 1
        //     ]);

        return response()->json(['voter' => $voter]);
        // if($request->isEligible == 1){
        //     $voting_code = $request->voter_code;    

        //     $isVotingCodeIsExists = DB::table('voting_codes')->where('voting_code', $voting_code)->exists();
        //     $isVotingCodeIsTaken = DB::table('voting_codes')->where('voting_code', $voting_code)->where('isTaken',1)->exists();
        //     if(!$isVotingCodeIsExists){
        //         return response()->json(['voting_code' => 'Voting Code does no exists.']);        
        //     }else if($isVotingCodeIsTaken){
        //         return response()->json(['voting_code' => 'Voting Code is already taken.']);        

        //     }else{
        //         $votingCodeDelete = DB::table('voting_codes')
        //             ->where('voting_code', $voter->voter_code)
        //             ->delete();
        //         $deleteVotes = DB::table('vote_details')
        //             ->where('voter_code', $voter->voter_code)
        //             ->delete();
        //         $voter->organization = $request->organization;
        //         $voter->first_name = $request->first_name;
        //         $voter->last_name = $request->last_name;
        //         $voter->designation = $request->designation;
        //         $voter->isEligible = $request->isEligible;
        //         $voter->voter_code = '';  //add validation later
        //         $voter->save();

        //         $votingCodeUpdate = DB::table('voting_codes')
        //             ->where('voting_code', $voting_code)
        //             ->update([
        //                 'isTaken' => 1
        //             ]);

        //         return response()->json(['voter' => $voter]);        

        //     }
        // }else{
        //         $votingCodeDelete = DB::table('voting_codes')
        //             ->where('voting_code', $voter->voter_code)
        //             ->delete();
        //         $deleteVotes = DB::table('vote_details')
        //             ->where('voter_code', $voter->voter_code)
        //             ->delete();
        //         $voter->organization = $request->organization;
        //         $voter->first_name = $request->first_name;
        //         $voter->last_name = $request->last_name;
        //         $voter->designation = $request->designation;
        //         $voter->isEligible = $request->isEligible;
        //         $voter->voter_code = '';  //add validation later
        //         $voter->save();


        //         return response()->json(['voter' => $voter]);                    
        // }
    }

    private function validateVoteCode()
    {
        return Str::random(4);
    }

    private function deleteVotingCode($voter)
    {
        $votingCodeDelete = DB::table('voting_codes')
            ->where('voting_code', $voter->voter_code)
            ->delete();
        $deleteVotes = DB::table('vote_details')
            ->where('voter_code', $voter->voter_code)
            ->delete();
    }

    function newVoteCode(Request $request)
    {
        $code = Str::random(4);
        $isVoteCodeExists = DB::table('voters')->where('voter_code', $code)->exists();
        $vote_code = '';
        if ($isVoteCodeExists) {
            $vote_code = Str::random(4);
        } else {
            $vote_code = $code;
        }
        return response()->json([
            'vote_code' => $vote_code
        ]);
    }

    function search(Request $request)
    {
        $string = $request->searchString;
        if ($string != '') {
            $voters = DB::table('voters')
                ->where('first_name', 'like', '%' . $string . '%')
                ->orWhere('last_name', 'like', '%' . $string . '%')
                ->orWhere('organization', 'like', '%' . $string . '%')
                ->orWhere('designation', 'like', '%' . $string . '%')
                ->orWhere('voter_code', 'like', '%' . $string . '%')->get();
        } else {
            $voters = DB::table('voters')->orderBy('id', 'desc')->get()->take(10);
        }

        return response()->json(['voters' => $voters]);
    }

    function updateVotingCode(Request $request)
    {
        $voter = Voter::find($request->voter_id);
        $voting_code = $request->voting_code;
        // return $request->voting_code;    
        $isVotingCodeIsExists = DB::table('voting_codes')->where('voting_code', $voting_code)->exists();
        $isVotingCodeIsTaken = DB::table('voting_codes')->where('voting_code', $voting_code)->where('isTaken', 1)->exists();
        if (!$isVotingCodeIsExists) {
            return response()->json(['voting_code' => 'Voting Code does no exists.']);
        } else if ($isVotingCodeIsTaken) {
            return response()->json(['voting_code' => 'Voting Code is already taken.']);
        } else {
            $this->deleteVotingCode($voter);
            $voter->voter_code = $voting_code;  //add validation later
            $voter->save();

            $votingCodeUpdate = DB::table('voting_codes')
                ->where('voting_code', $voting_code)
                ->update([
                    'isTaken' => 1
                ]);

            return response()->json(['voter' => $voter]);
        }
    }

    function assignVotingCode()
    {
        $noVotingCodeVoter = DB::table('voters')->where('voter_code', null)->orWhere('voter_code', '')->get();
        // $availableVotingCode = DB::table('voting_codes')->where('isTaken', 0)->get()->take(2);


        foreach ($noVotingCodeVoter as $key => $value) {
            $availableVotingCode = DB::table('voting_codes')->where('isTaken', 0)->value('voting_code');
            $voter = Voter::find($value->id);
            $voter->voter_code = $availableVotingCode;
            $voter->save();
            $updateVotingCode = DB::table('voting_codes')->where('voting_code', $availableVotingCode)->update(['isTaken' => 1]);
        }
        // foreach ($availableVotingCode as $avotingCode) {
        //     foreach ($noVotingCodeVoter as $key => $nVoter) {
        //         $voter = Voter::find($nVoter->id);
        //         $voter->voter_code = $avotingCode->voting_code;
        //         $vc = DB::table('voting_codes')->where('voting_code', $avotingCode->voting_code)->update(['isTaken' => 1]);
        //         $voter->save();
        //     }
        // }

        return response()->json(['status' => 'ok']);
    }


    public function email(Request $request)
    {
        $voters = Voter::where('is_emailed', '0')->get();
        foreach ($voters as $key => $value) {

            $voter = Voter::find($value->id);
            if ($voter->is_emailed == 0) {
                // email registrant
                $to_email = trim($voter->email);
                $data = array("voter" => $voter, 'host' => $request->root());
                Mail::send('emails.votermail', $data, function ($message) use ($to_email) {
                    $message->to($to_email)
                        ->subject('PCQACL Online Voting');
                    $message->from('vote.system20@gmail.com', 'PCQACL Online Voting');
                });
                $voter->is_emailed = 1;
                $voter->save();
            }
        }
    }

    public function getVoters()
    {
        $guzzle = new \GuzzleHttp\Client;

        $token = $guzzle->post('https://eventsv2.psp.com.ph/oauth/token', [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => 3,
                'client_secret' => 'kVeuoxV4MrWnT9LlISrQfbWoEoSAQoqh4mmc7sY4',
                'scope' => '*',
            ],
        ]);
        // return json_decode((string) $token->getBody(), true)['access_token'];

        $response = $guzzle->get('https://eventsv2.psp.com.ph/api/users', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . json_decode((string) $token->getBody(), true)['access_token'],
            ],

        ]);
        $result = json_decode((string) $response->getBody(), true);
        if ($result) {
            foreach ($result['users'] as $key => $value) {
                if ($result['users'][$key]['classification'] == 'Diplomate' || $result['users'][$key]['classification'] == 'Fellow') {

                    $user = Voter::firstOrCreate(
                        [
                            'registrant_id' => $result['users'][$key]['id'],
                        ],
                        [
                            'organization' => $result['users'][$key]['affiliation'],
                            'designation' => $result['users'][$key]['classification'],
                            'registrant_id' => $result['users'][$key]['id'],
                            'name' => $result['users'][$key]['first_name'] . " " . $result['users'][$key]['last_name'],
                            'first_name' => $result['users'][$key]['first_name'],
                            'last_name' => $result['users'][$key]['last_name'],
                            'email' => $result['users'][$key]['email'],
                            'username' => $result['users'][$key]['username'],
                            'isEligible' => 0,
                        ]
                    );
                }
            }

            // $user->assignRole('guest');
            return response()->json([
                'status' => 'ok',
                // 'user' => $user,
                // 'access_token' => $user->api_token,
            ]);
        } else {

            return response()->json([
                'status' => 'failed',
            ]);
        }
    }

    public function export(Request $request)
    {
        if ($request->candidate_id) {
            $candidate = \App\Candidate::find($request->candidate_id);
            // return \App\VoteDetail::with('voter')->where('candidate_id', $candidate->id)->get(['candidate_id', 'voter_code']);
            return Excel::download(new VotersVoteToExport($request->candidate_id), 'voters_vote_to_' . $candidate->first_name . '.xlsx');
        }
        return Excel::download(new VotersExport, 'voters.xlsx');
        // return Voter::withCount('votes')->get();
    }
}
