<?php

namespace App\Http\Controllers;

use App\Candidate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CandidateController extends Controller
{

    function all(Request $request){
        $voteCount = DB::table('setups')->where('id',1)->value('vote_count');
        $isMasked = DB::table('settings')->where('id',1)->value('isMasked');
        if($request->view == 1){
            $candidates = Candidate::withCount('candidate_votes')->orderBy('candidate_votes_count', 'desc')->get()->take($voteCount);
        }else{
            $candidates = DB::table('candidates')->get();

        }
        foreach ($candidates as $key => $value) {
            $candidates[$key]->votes = DB::table('vote_details')->where('candidate_id', $value->id)->count();
            # code...
        }        
        return response()->json(['candidates' => $candidates, 'ismasked' => $request->view]);        
    }

    function allCandidatesForAdmin(Request $request){
        $voteCount = DB::table('setups')->where('id',1)->value('vote_count');
        $isMasked = DB::table('settings')->where('id',1)->value('isMasked');
        if($request->view == 1){
            $candidates = Candidate::withCount('candidate_votes')->orderBy('candidate_votes_count', 'desc')->get();
        }else{
            $candidates = DB::table('candidates')->get();

        }
        foreach ($candidates as $key => $value) {
            $candidates[$key]->votes = DB::table('vote_details')->where('candidate_id', $value->id)->count();
            # code...
        }        
        return response()->json(['candidates' => $candidates, 'ismasked' => $request->view]);         
    }

    function index(){
    	return view('candidates.index', ['candidates' => DB::table('candidates')->orderBy('id', 'desc')->get()]);
    }

    function create(){
    	return view('candidates.create');

    }

    function save(Request $request){
    	$first_name = $request->first_name;
        $last_name = $request->last_name;
    	$organization = $request->organization;
        $imageName = $first_name."_".$last_name.'.'.$request->file->getClientOriginalExtension();
        $request->file->move(public_path('uploads/candidates'), $imageName); 
        $candidate = Candidate::create([
    			'first_name' => $first_name,
                'last_name' => $last_name,
    			'organization' => $organization,
    			'dp' => '/uploads/candidates/'.$first_name."_".$last_name.'.'.$request->file->getClientOriginalExtension()
    		]);   	
    	// $candidate = DB::table('candidates')
    	// 	->insert([
    	// 		'first_name' => $first_name,
    	// 		'last_name' => $last_name,
    	// 		'dp' => '/uploads/candidates/'.$first_name."_".$last_name.'.'.$request->file->getClientOriginalExtension()
    	// 	]);


    	return response()->json(['candidate' => $candidate]);
    }

    function show($id){
    	$candidate = Candidate::find($id);
    	return view('candidates.show', ['candidate'=> $candidate]);

    }

    function update(Request $request){
    	$candidate = Candidate::find($request->candidate_id);
    	$candidate->first_name = $request->first_name;
        $candidate->last_name = $request->last_name;
    	$candidate->organization = $request->organization;
    	if($request->file != null){
	        $imageName = $request->first_name."_".$request->last_name.'.'.$request->file->getClientOriginalExtension();
	        $request->file->move(public_path('uploads/candidates'), $imageName); 
	        $candidate->dp = '/uploads/candidates/'.$request->first_name."_".$request->last_name.'.'.$request->file->getClientOriginalExtension();
    	}
    	$candidate->save();

    	return response()->json(['candidate' => $candidate]);
    }    

    function ranking(){
        $candidates = DB::table('candidates')->inRandomOrder()->get();
        $isMasked = DB::table('settings')->where('id',1)->value('isMasked');

        // $candidates = Candidate::withCount('candidate_votes')->orderBy('candidate_votes_count', 'desc')->get();
        foreach ($candidates as $key => $value) {
            $candidates[$key]->votes = DB::table('vote_details')->where('candidate_id', $value->id)->count();
            # code...
        }
        return view('candidates.ranking', ['candidates' => $candidates, 'ismasked' => $isMasked]);
    }
    function adminRanking(){
        $candidates = DB::table('candidates')->inRandomOrder()->get();
        $isMasked = DB::table('settings')->where('id',1)->value('isMasked');

        // $candidates = Candidate::withCount('candidate_votes')->orderBy('candidate_votes_count', 'desc')->get();
        foreach ($candidates as $key => $value) {
            $candidates[$key]->votes = DB::table('vote_details')->where('candidate_id', $value->id)->count();
            # code...
        }
        return view('candidates.adminRanking', ['candidates' => $candidates, 'ismasked' => $isMasked]);
    }    

    public function delete($candidate_id){
        $destroy = Candidate::destroy($candidate_id);
        return response('',201);
    }
}
