<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Setting;
use App\VotingCode;
use Illuminate\Support\Str;

class SettingController extends Controller
{
    function index(){
        $setup = DB::table('setups')->first();
        $voting_codes = VotingCode::select('voting_code')->get();
		// dd($voting_codes);
        foreach($voting_codes as $key => $value) {
            $voting_codes[$key]->voter = DB::table('voters')->select('organization')->where('voter_code', $value->voting_code)->first();
        }
    	$settings = DB::table('settings')->where('id', 1)->first();
    	return view('settings.index', ['settings' => json_encode($settings), 'setup' => json_encode($setup), 'voting_codes' => $voting_codes]);
    }

    function save(Request $request){
    	$settings = DB::table('settings')
    		->where('id', 1)
    		->update([
    			'isMasked' => $request->isMasked,
    			'start_period' => $request->start_period,
    			'end_period' => $request->end_period,
    			'not_yet_message' => $request->not_yet_message,
    			'closed_message' => $request->closed_message,
    		]);
		if($request->quantity > 0){

			for ($i=0; $i < $request->quantity; $i++) { 
				$code = Str::random(6);
				$isCodeExists = DB::table('voters')->where('voter_code', $code)->exists();
				if($isCodeExists){
					$voter_code = Str::random(6);
				}else{
					$voter_code = $code;
				}
				VotingCode::create([
					'voting_code' => $voter_code
				]);
			}  
		}
		$setup = DB::table('setups')
    		->where('id', 1)
    		->update([
    			'vote_count' => $request->vote_count,
    		]);
    	return response()->json(['status' => 'success']);
    }


}
