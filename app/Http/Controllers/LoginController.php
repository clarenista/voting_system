<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function index(){
        return view('login.index');
    }

    public function login(Request $request){
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            // return 'yes';
            // return redirect()->intended('admin/overview');         
        }else{
        	return response()->json([
        		'status' => 'failed',

        	], 400);
        }
    }

    function verify(Request $request){
        $admin = DB::table('users')->first();
        $password = $request->password;

        $isPasswordExists = Hash::check($password, $admin->password);
        return response()->json(['isExists' => $isPasswordExists]);
    }
}
