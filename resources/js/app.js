/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import { store } from './store/store'

import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import axios from 'axios'

Vue.use(BootstrapVue)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('setup-component', require('./setup/Setup.vue').default);
Vue.component('cast-vote', require('./components/CastVote.vue').default);
Vue.component('cast-vote-form', require('./components/CastVoteForm.vue').default);
Vue.component('cast-vote-select', require('./components/CastVoteSelect.vue').default);
Vue.component('login-component', require('./components/Login.vue').default);
Vue.component('candidates-ranking', require('./components/CandidateRanking.vue').default);
Vue.component('period-message', require('./components/PeriodMessage.vue').default);

Vue.component('admin-candidates-list', require('./admin/CandidateList.vue').default);
Vue.component('admin-create-candidates', require('./admin/NewCandidate.vue').default);
Vue.component('admin-show-candidate', require('./admin/Candidate.vue').default);
Vue.component('admin-voters-list', require('./admin/VoterList.vue').default);
Vue.component('admin-create-voter', require('./admin/NewVoter.vue').default);
Vue.component('admin-settings-component', require('./admin/Settings.vue').default);
Vue.component('admin-ranking', require('./admin/Ranking.vue').default);



Vue.component('modal-component', require('./components/Modal.vue').default);
Vue.component('bs4-modal', require('./components/Bs4Modal.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store,
    created(){
    	// this.getSetup()
    },
    methods:{
        getSetup(){
    	    axios.get('/setup/show')
    	    	.then(({data})=>{
                    console.log('app.js',data)
        			this.$store.commit('updateSetup', data.setup)
    	    	});
    	}
    },

});
