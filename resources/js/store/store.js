import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    setupPage: 1,
    text_companyName: '',
    text_voteCount: '',
    fileLogo: '',
    fileEventLogo: '',
    text_name: '',
    text_email: '',
    text_password: '',
    vc: '',
    isVoteCodeExists: false,
    candidates: [],
    allCandidates: [],
    voter:[],
    setup: null
  },
  mutations: {
  	// change page
    changeSetupPage(state, setupPage) {
      state.setupPage = setupPage
    },

    // change company name
    changeCompanyName(state, text_companyName){
        state.text_companyName = text_companyName
    },

    changeVoteCount(state, text_voteCount){
    	state.text_voteCount = text_voteCount

    },

    // change company logo
    changeCompanyLogo(state, file_logo){
    	state.fileLogo = file_logo
    	// console.log(file_logo)
    },  

    // change event logo
    changeEventLogo(state, file_event_logo){
    	state.fileEventLogo = file_event_logo
    	// console.log(file_logo)
    },      

    // change company admin name
    changeAdminName(state, text_name){
    	state.text_name = text_name
    },      

    // change company admin email
    changeAdminEmail(state, text_email){
    	state.text_email = text_email
    },         

    // change company admin password
    changeAdminPassword(state, text_password){
    	state.text_password = text_password
    }, 

    // save setup
	handleSubmitNew(){
		let fd = new FormData();
        fd.append('company_name', this.state.text_companyName);
		fd.append('vote_count', this.state.text_voteCount);
		fd.append('file_logo', this.state.fileLogo);
		fd.append('file_event_logo', this.state.fileEventLogo);
		fd.append('name', this.state.text_name);
		fd.append('email', this.state.text_email);
		fd.append('password', this.state.text_password);

		axios
			.post('setup/save', fd)
			.then(({data})=>{
				window.location.href = "/";
			})
	},

    // check Vote Code
    async checkMyVoteCode(state, voteCode){
        state.vc = ''
        let fd = new FormData();
        fd.append('vote_code', voteCode);        
        let {data} = await axios.post('check-vote-code', fd)
        // console.log(data)
        if(data.voter){
            state.vc = voteCode
            state.isVoteCodeExists = true
            state.candidates = data.candidates
            state.voter = data.voter
        }else{
            state.isVoteCodeExists = false
            state.vc = null

        }
    },

        // check Username
        async checkMyUsername(state, username){
            state.vc = ''
            let fd = new FormData();
            fd.append('username', username)
            try{
                let {data} = await axios.post('check-username', fd);
                if(data.voter){
                    if(data.voter.voter_code != null){
                        
                        state.candidates = data.candidates
                        state.voter = data.voter
                        setTimeout(()=>{
                            state.vc = data.voter.voter_code
                        }, 2000)
                    }else{
                        
                        state.isVoteCodeExists = false
                        state.vc = null
                    }
                    
                }else{
                    state.isVoteCodeExists = false
                    state.vc = null
        
                }
            }catch({response}){
                if(response.status == 401){
                    state.vc = null
                }else{
                    alert(response.statusText)
                }
            }
            
            
        },

    candidatesToStore(state, allCandidates){
        state.allCandidates = allCandidates
    },
    handleGoBackClicked(state, voteCode){
        state.vc = voteCode

    },
    pushNewCandidate(state, candidate){
        state.allCandidates.unshift(candidate)
    },
    updateSetup(state, setup){
        state.setup = setup

    },
    updateStoreVc(state, vc){
        state.vc = vc
    }

  },
  getters: {
    setupPage: state => state.setupPage,
    text_companyName: state => state.text_companyName,
    text_voteCount: state => state.text_voteCount,
    file_logo: state => state.fileLogo,
    text_name: state => state.text_name,
    email: state => state.email,
    password: state => state.password,
    isVoteCodeExists: state => state.isVoteCodeExists,
    vc: state => state.vc,
    candidates: state => state.candidates,
    allCandidates: state => state.allCandidates,
    voter: state => state.voter,
    setup: state => state.setup,
  }
})