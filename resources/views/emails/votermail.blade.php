<p>Dear {{$voter->first_name ? $voter->first_name : $voter->organization.' Representative'}},</p><br>

<br>
<p>
You received this email because you are eligible to vote as the official representative of {{$voter->organization}} during the virtual election for the members of the PCQACL Board of Trustees. Voting will open at 8:00AM of September 22, 2021 and will close at 5:00 PM September 23, 2021. 
To cast your vote, please click the link below:
</p>
<p>
	<a href="{{$host}}/cast-vote?vcode={{$voter->voter_code}}">{{$host}}/cast-vote?vcode={{$voter->voter_code}}</a>
</p>
<p>Your voting code is: <u>{{$voter->voter_code}}</u></p>
<p>Do not share this unique code. </p>

<br>
<p>Thank you.</p>
<p>Sincerely,</p>
<p></p>
<p>NOMELEC PCQACL Virtual Election</p>