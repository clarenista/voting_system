@extends('layouts.app')

@section('title', 'Voting System Overview')

@section('content')
	<div class="container mt-5" >
		<div class="row">
			<!-- Cast Vote -->
			<div class="col">
				<a href="/cast-vote">
					<div class="card rounded-0 border-info">
					  <div class="card-body">
						<div class="row ">
							<div class="col d-flex justify-content-center">
								<i class="fas fa-vote-yea  text-info fa-10x"></i>
							</div>
							<hr>
						</div>
						<hr>
						<div class="row">
							<div class="col d-flex justify-content-center">
								<h3 class="text-info ">Voting & Canvassing</h3>
							</div>
						</div>
						<div class="row">
							<div class="col d-flex justify-content-center">
								<p>Casting of votes</p>
							</div>
						</div>						
					  </div>
					</div>				
				</a>				
			</div>				
		</div>
	</div>
@endsection