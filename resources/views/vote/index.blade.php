@extends('layouts.app')

@section('title', 'Cast vote')

@section('content')
	<div class="container mt-2" >
		
		<cast-vote :props_settings="{{$settings}}" ></cast-vote>
		
	</div>
@endsection