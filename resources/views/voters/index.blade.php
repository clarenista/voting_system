@extends('layouts.admin')

@section('title', 'Voters')

@section('content')
	<admin-voters-list :voters="{{$voters}}" :available_vc="{{$available_vc}}"></admin-voters-list>

@endsection