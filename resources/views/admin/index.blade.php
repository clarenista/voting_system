@extends('layouts.admin')

@section('title', 'Admin Overview')

@section('content')
	
	<div class="container mt-5">	
		<div class="row justify-content-center">
			<div class="col-6">
				
				<div class="row">
					<div class="col">
						<a href="/candidates/admin-ranking">
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Ranking</h4>
								</div>
							</div>
						</a>
					</div>					
					<div class="col">
						<a href="/candidates">
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Candidates</h4>
								</div>
							</div>
						</a>
					</div>
					<div class="col">
						<a href="/voters">
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Voters</h4>
								</div>
							</div>
						</a>
					</div>			
				</div>
			</div>
			
		</div>
	</div>

@endsection