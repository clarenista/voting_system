@extends('layouts.app')

@section('title', 'Candidates')

@section('content')
	<candidates-ranking 
		:candidates="{{$candidates}}"
		:ismasked="{{$ismasked}}"
	></candidates-ranking>


@endsection