@extends('layouts.admin')

@section('title', 'Ranking')

@section('content')
	<admin-ranking 
		:candidates="{{$candidates}}"
		:ismasked="{{$ismasked}}"
	></admin-ranking>


@endsection