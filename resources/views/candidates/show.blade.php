@extends('layouts.admin')

@section('title', 'Show Candidate')

@section('content')
	<admin-show-candidate :candidate="{{$candidate}}"></admin-show-candidate>

@endsection