@extends('layouts.admin')

@section('title', 'Candidates')

@section('content')
	
	<admin-candidates-list :candidates="{{$candidates}}"></admin-candidates-list>

@endsection