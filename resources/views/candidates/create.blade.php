@extends('layouts.admin')

@section('title', 'Create new Candidates')

@section('content')
	<admin-create-candidates></admin-create-candidates>

@endsection