<nav class="navbar navbar-expand-lg text-light navbar-dark bg-primary">
  <div class="container">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="/admin/overview">Overview <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/candidates/admin-ranking">Ranking</a>
        </li>        
        <li class="nav-item">
          <a class="nav-link" href="/candidates">Candidates</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/voters">Voters</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/settings">Settings</a>
        </li>        
      </ul>
      <form class="form-inline my-2 my-lg-0" method="get" action="/admin/logout">
        <button class="btn btn-danger my-2 my-sm-0"><i class="fa fa-sign-out">  </i>Logout</button>
      </form>
    </div>
   </div>
</nav>   