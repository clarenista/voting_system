<div class="container">
    <a href="/">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-2 col-sm-2 my-3 d-none d-sm-block ">
                <img src="{{$setup->company_logo}}" class="img-fluid" alt="">
            </div>
            <div class="col-md-8 py-0 col-sm-8 text-center">
                <h1>{{$setup->company_name}} </h1>
                <p class="lead">Voting System</p>
                
            </div>
            <div class="col-md-2 my-3 col-sm-2 d-none d-sm-block">
                <img src="{{$setup->file_event_logo}}" class="img-fluid" alt="">
            </div>            
        </div>            
    </a>
    <hr>
</div>