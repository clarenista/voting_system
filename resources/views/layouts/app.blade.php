<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>
    @yield('title')
  </title>

  <link rel="stylesheet" href="{{asset('css/app.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css">
</head>

<body>
  <div id="app">
    @include('layouts.app_header')

    @yield('content')
  </div>
  <footer id="sticky-footer" class="py-4 bg-light text-dark-50">
    <div class="container text-center">
      <small>Powered by: <img style="width: 10%" src="{{asset('./images/bobongMD.png')}}" alt=""></small>
    </div>
  </footer>
  <script src="{{asset('js/app.js')}}?v0.01"></script>
</body>

</html>