@extends('layouts.admin')

@section('title', 'Settings')

@section('content')
	<admin-settings-component 
		:votingcodes="{{$voting_codes}}"
		:settings="{{$settings}}"
		:setup="{{$setup}}"
	></admin-settings-component>

@endsection