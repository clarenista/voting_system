<?php

use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SetupController@index');
Route::get('/ranking', 'CandidateController@ranking');
Route::get('/cast-vote', 'VoteDetailController@index');
Route::post('/verification', 'LoginController@verify');
Route::get('/admin/overview', 'HomeController@overview')->name('home');
Route::get('/admin/logout', 'HomeController@logout');

// guest
Route::post('setup/save', 'SetupController@save');
Route::get('setup/show', 'SetupController@show');
Route::post('check-vote-code', 'VoterController@checkVoteCode');
Route::post('check-username', 'VoterController@checkusername');
Route::post('candidates/allCandidates', 'CandidateController@all');
Route::post('votes/save', 'VoteDetailController@save');

Route::get('/getvoters', 'VoterController@getVoters');

// candidate module
Route::get('/candidates', 'CandidateController@index')->middleware('auth');
Route::post('candidates/allCandidatesForAdmin', 'CandidateController@allCandidatesForAdmin');
Route::get('/candidates/admin-ranking', 'CandidateController@adminRanking')->middleware('auth');
Route::get('/candidates/{id}', 'CandidateController@show');
Route::post('/candidates/{id}/delete', 'CandidateController@delete');
// Route::get('/candidates/ranking', 'CandidateController@adminRanking')->middleware('auth');
Route::post('candidates/save', 'CandidateController@save')->middleware('auth');
Route::post('candidates/update', 'CandidateController@update')->middleware('auth');

// voters module
Route::get('/voters', 'VoterController@index')->middleware('auth');
Route::get('/voters/email', 'VoterController@email')->middleware('auth');
Route::post('voters/save', 'VoterController@save')->middleware('auth');
Route::post('voters/update', 'VoterController@update')->middleware('auth');
Route::post('voters/new-vote-code', 'VoterController@newVoteCode')->middleware('auth');
Route::post('voters/search', 'VoterController@search')->middleware('auth');
Route::post('voters/updateVotingCode', 'VoterController@updateVotingCode')->middleware('auth');
// Route::get('voters/assign-voting-code', 'VoterController@assignVotingCode')->middleware('auth');

Route::get('voters/export', 'VoterController@export')->middleware('auth');
// settings module
Route::get('settings', 'SettingController@index')->middleware('auth');
Route::post('settings/add_vote_codes', 'SettingController@addVoteCodes')->middleware('auth');
Route::post('settings/save', 'SettingController@save')->middleware('auth');


Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
