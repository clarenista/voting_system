<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::post('setup/save', 'SetupController@save');
Route::post('check-vote-code', 'VoterController@checkVoteCode');
// Route::post('votes/save', 'VoteDetailController@save');
// Route::post('candidates/save', 'CandidateController@save');
// Route::post('candidates/update', 'CandidateController@update');
// Route::post('candidates/allCandidates', 'CandidateController@all');

// Route::post('voters/save', 'VoterController@save');
// Route::post('voters/update', 'VoterController@update');
// Route::post('voters/new-vote-code', 'VoterController@newVoteCode');

// Route::post('login', 'LoginController@login');
// Route::post('login', [ 'as' => 'admin-login', 'uses' => 'LoginController@login']);