<?php

use Illuminate\Database\Seeder;
use App\Setting;
use App\Voter;
use App\VotingCode;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();
        $tomorrow = $now->addDays(1);
        $faker = Faker\Factory::create();        
        Setting::create([
        	'isMasked' => 0,
            'start_period' => $now,
            'end_period' => $tomorrow
        ]);

        for ($i=0; $i < 1000; $i++) { 
            $code = Str::random(6);
            $isCodeExists = DB::table('voters')->where('voter_code', $code)->exists();
            if($isCodeExists){
                $voter_code = Str::random(6);
            }else{
                $voter_code = $code;

            }
            VotingCode::create([
                'voting_code' => $voter_code
            ]);
        }        

        // populate purposes only

        // for ($i=0; $i < 1000; $i++) { 
        //     $code = Str::random(4);
        //     $isCodeExists = DB::table('voters')->where('voter_code', $code)->exists();
        //     if($isCodeExists){
        //         $voter_code = Str::random(4);
        //     }else{
        //         $voter_code = $code;

        //     }
        //     Voter::create([
        //         'first_name' => $faker->firstName,
        //         'last_name' => $faker->lastName,
        //         'isEligible' => 1,
        //         'voter_code' => $voter_code
        //     ]);
        // }
        $this->call([
            VoterSeeder::class,
        ]);
    }
}
