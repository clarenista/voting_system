<?php

use Illuminate\Database\Seeder;
use App\Voter;

class VoterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Voter::truncate();

        $csvFile = fopen(base_path("database/csv/voters.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                // $voterExists = Voter::where('email', $data['2'])->exists();
                Voter::create([
                    "last_name" => utf8_encode($data['0']),
                    "first_name" => utf8_encode($data['1']),
                    // "email" => utf8_encode($data['2']),
                    // "designation" => $data['3'],
                    // "organization" => utf8_encode($data['4']) ?? '',
                    'isEligible' => 1
                ]);
                // if (!$voterExists) {

                // }
            }
            $firstline = false;
        }

        fclose($csvFile);
    }
}
